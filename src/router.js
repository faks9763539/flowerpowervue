import { createRouter, createWebHistory } from 'vue-router';

import HomePage from './pages/HomePage.vue';
import OrderPage from './pages/OrderPage.vue';
import AboutUsPage from './pages/AboutUsPage.vue';
import CartPage from './pages/CartPage.vue';

// kreiranje router-a i postavljanje URL-ova
const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', redirect: '/home' },
        { path: '/home', component: HomePage },
        { path: '/order', component: OrderPage },
        { path: '/aboutus', component: AboutUsPage },
        { path: '/cart', component: CartPage }
    ]
});

export default router;